# VHT-PHPExcel #

A pure PHP library for reading and writing spreadsheet files

# Example #
```
  test/index.php
```

# More example #

You could find a lot of examples in the official PHPExcel repository https://github.com/PHPOffice/PHPExcel/tree/develop/Examples

Credits: https://github.com/liuggio/ExcelBundle